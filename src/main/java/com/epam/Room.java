package com.epam;

public class Room extends Accomodation {
    String typeOfBuilding;
    public Room(String name, double price, int square, double distanceToTransport,
                boolean infrastructure, Enum district, Enum typeOfBuilding) {
        super(name, price, square, distanceToTransport, infrastructure, district);
        this.typeOfBuilding = typeOfBuilding.toString();
    }
}
