package com.epam;

public class Mansion extends Accomodation {
    private int numberOfFloors;
    private int numberOfRooms;
    boolean pool;

    public Mansion(String name, double price, int square, double distanceToTransport,
                   boolean infrastructure, Enum district, int numberOfFloors, int numberOfRooms, boolean pool) {
        super(name, price, square, distanceToTransport, infrastructure, district);
        this.numberOfFloors = numberOfFloors;
        this.numberOfRooms = numberOfRooms;
        this.pool = pool;
    }
}
