package com.epam;

public class Flat extends Accomodation {
    public int numberOfRooms;
    public Flat(String name, double price, int square, double distanceToTransport,
                boolean infrastructure, Enum district, int numberOfRooms) {
        super(name, price, square, distanceToTransport, infrastructure, district);
        this.numberOfRooms = numberOfRooms;
    }
}
