package com.epam;

public class Penthouse extends Accomodation {
    int floor;
    boolean pool;

    public Penthouse(String name, double price, int square, double distanceToTransport,
                     boolean infrastructure, Enum district, int floor, boolean pool) {
        super(name, price, square, distanceToTransport, infrastructure, district);
        this.floor = floor;
        this.pool = pool;
    }

    public int getFloor() {
        return floor;
    }

    public boolean getPool() {
        return pool;
    }
}
