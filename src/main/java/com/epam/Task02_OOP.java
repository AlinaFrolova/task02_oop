package com.epam;

import java.util.ArrayList;
import java.util.List;

import static com.epam.District.*;
import static com.epam.TypeOfBuilding.FLAT;
import static com.epam.TypeOfBuilding.HOSTEL;

public class Task02_OOP {
    public static void main(String[] args) {
        List<Accomodation> accomodation = new ArrayList();
        accomodation.add(new Room("room1", 200000, 14, 2.0,
                true,  GALITSKII, HOSTEL ));
        accomodation.add(new Room("room2", 140000, 10, 3.6,
                false,  ZALIZNICHNII, FLAT ));
        accomodation.add(new Flat("flat1", 1500000, 45, 1.5,
                true, LICHAKIVSKII, 3));
        accomodation.add(new Flat("flat2", 2350000, 38, 0.8,
                false, FRANKIVSKII, 2));
        accomodation.add(new Penthouse("penthouse1", 2330000, 39, 2.4,
                true, SIHIVSKII, 16, true));
        accomodation.add(new Penthouse("penthouse2", 3170000, 47, 1.6,
                true, SIHIVSKII, 100, false));
        accomodation.add( new Mansion("mansion1", 3123000, 56, 1.2,
                false, PIVDENNII, 3,8, true ));
        accomodation.add( new Mansion("mansion2", 4520000, 39, 0.7,
                true, GALITSKII, 2,10, false ));
    }
}
