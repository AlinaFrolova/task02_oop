package com.epam;

public enum TypeOfBuilding {
    PENTHOUSE, FLAT, MANSION, HOSTEL
}
