package com.epam;

public class Accomodation {

    private String name;
    private double price;
    private int square;
    private double distanceToTransport;
    private boolean infrastructure;
    private String district;

    public Accomodation(String name, double price, int square, double distanceToTransport,
                        boolean infrastructure, Enum district){
        this.name = name;
        this.price = price;
        this.square = square;
        this.distanceToTransport = distanceToTransport;
        this.infrastructure = infrastructure;
        this.district = district.toString();
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getSquare() {
        return square;
    }

    public double getDistanceToTransport(){
        return distanceToTransport;
    }

    public boolean getInfrastructure(){
        return infrastructure;
    }

}
